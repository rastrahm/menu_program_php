<?php
    include_once('bibliotecas.php');
    /*
    * Enrutador basico
    * Maneja según el URL las diferentes peticiones las rutas
    * GET sin parametros: Trae la ventana de Login
    * POST con pwd y user: Verifica al usuario y retorna: jwt, menu (estructura)
    * GET con jwt: Verifica la valides y retorna jwt, menu (estructura)
    * Todas las funciones a continuación validan el JWT antes de iniciarce
    * Las fnc_ hacen referecia a funciones guardadas en la base de datos que se optienen a través de la funcion Object del objeto Security
    * GET: /options/ Trae todas las filas en consecuencia del fnc_ definido en la base de datos
    * GET: /options/{id} Trae el registro identificado por el id de la fnc_ definida en la base de datos
    * POST: /options/ Genera un nuevo registro usando la fnc_ definida
    * PUT: /options/{id}: Guarda modificaciones al registro {id} usando la fnc_ definida
    * DELETE: /options/{id}: Elimina el registro identificado por {id} usando la fnc_ definida
    */
    // Separa la ruta
    function paths($url) {
        $uri = parse_url($url);
        return $uri['path'];
    }
    /**
     * result
     * Enviá como JSON el resultado de la consulta y establece el response code
     * @param resultado object con el resultado de la pagina
     * @param int_set_header integer con el status de la pagina
     * @return respuesta del web service
     */
    function result($resultado, $int_set_header) {
        if ($int_set_header == 200){
            http_response_code(200);
            header('Content-type: application/json');
        } else {
            http_response_code($int_set_header);
        }
        echo $resultado;
    }

    /**
     * getAllMenus
     * Buscamos todos los elementos del menú
     * @param profile id del perfil para el menú
     * @return array con los elementos del menú
     */
    function getAllMenus($profile) {
        $db = New Conn();
        $menustub = New menu();
        $arr_menustub = array();
        $arr_menustub['menus'] = $db->Select($menustub->getMenu($profile));
        foreach ($arr_menustub['menus'] as $key => $value) {
            $arr_menustub['menus'][$key]['str_submenu'] = $db->Select($menustub->getSubmenu($profile, $value['big_id']));
        }
        return $arr_menustub;
    }

    /**
     * handle_index
     * Maneja las petidiones get y post del indice
     * @param method string con el metodo de acceso (POST, GET, OPTION, PUT)
     * @return objeto con los elementos del front
     */
    function handle_index($method) {
        $db = New Conn();
        $security = New security();
        $jwtsecur = New jwtoken();
        $rawData = json_decode(file_get_contents("php://input"),true);
        switch($method) {
            case 'GET':
                $arr_header = apache_request_headers();
                if ($arr_header and array_key_exists('jwt', $arr_header)) {
                    $obj_security = $jwtsecur->GetJWT($arr_header['jwt'], $db->Select($security->KeyJWT()));
                    if (gettype($obj_security) == 'object') {
                        $arr_security = array("id" => $obj_security->data->id, "user" => $obj_security->data->user);
                        $arr_security = $db->Select($security->Verify($arr_security));
                        if ($arr_security[0]['result']=='t') {
                            $arr_response['data'] = array('id' => $obj_security->data->id, "profile" => $obj_security->data->profile);
                            $arr_response['jwt'] = $jwtsecur->SetJWT($obj_security->data->profile, $obj_security->data->user, $obj_security->data->profile, $db->Select($security->KeyJWT()));
                            $arr_response['menu'] = getAllMenus($obj_security->data->profile);
                            $int_set_header = 200;
                        } else {
                            $int_set_header = 404;
                            $arr_response = array('message' => 'Intento de ingreso invalido');
                        }
                    } else {
                        $int_set_header = 401;
                        $arr_response = array('message' => $obj_security[1]);
                    }
                } else {
                    $arr_response = array();
                    $int_set_header = 200;
                }
                result(json_encode($arr_response), $int_set_header);
                break;
            case 'POST':
                $arr_response = array();
                $arr_response['data'] = $db->SelectUser($security->Select($rawData));
                $arr_data = $arr_response['data'];
                if (!array_key_exists('message', $arr_data)) {
                    // JWT
                    $arr_response['jwt'] = $jwtsecur->SetJWT($arr_response['data'][0]['big_id'], $rawData['user'], $arr_response['data'][0]['big_profile'], $db->Select($security->KeyJWT()));
                    $arr_response['menu'] = getAllMenus($arr_response['data'][0]['big_profile']);
                    $arr_response['dictionary'] = $db->Select($security->Dictionary());
                    $int_set_header = 200;
                } elseif ($arr_data['message'] == 'Usuario no valido') {
                    $int_set_header = 401;
                } else {
                    $int_set_header = 404;
                }
                result(json_encode($arr_response), $int_set_header);
                break;
            default:
                // Header en caso de error
                header('HTTP/1.1 405 Method Not Allowed');
                header('Allow: GET');
                break;
        }
    }

    // Maneja las peticiones get y post de salida
    function handle_salir($method) {
    }

    /**
     * handle_opciones_method
     * Meneja las periticiones de post, put, get y delete con parametros require el JWT
     * @param method string con el metodo de acceso (POST, GET, OPTION, PUT)
     * @param name string ruta que determina la accion a realizar
     * @param stId string con el Id del recurso a utilizar
     * @return objeto con respuesta del servicio
     */
    function handle_opciones_method($method, $name, $stId) {
        // Declaramos los objetos basico de base de datos ($db), seguridad ($security), token ($jwtsecur)
        $db = New Conn();
        $security = New security();
        $jwtsecur = New jwtoken();
        $rawData = json_decode(file_get_contents("php://input"),true);
        if ($rawData == null) {
            $rawData['menu'] = $name;
        }
        $arr_header = apache_request_headers();
        // Verificamos que exista el header con el token
        if ($arr_header and array_key_exists('jwt', $arr_header)) {
            $obj_security = $jwtsecur->GetJWT($arr_header['jwt'], $db->Select($security->KeyJWT()));
            if (gettype($obj_security) == 'object') {
                $arr_security = array("id" => $obj_security->data->id, "user" => $obj_security->data->user);
                $arr_security = $db->Select($security->Verify($arr_security));
                if ($arr_security[0]['result']=='t') {
                    // Hacemos la busqueda del objeto según la opción pasada
                    $arr_parameter = array();
                    $arr_parameter = $db->Select($security->Object($obj_security->data->profile, $rawData['menu']));
                    $arr_response['jwt'] = $jwtsecur->SetJWT($obj_security->data->id, $obj_security->data->user, $obj_security->data->profile, $db->Select($security->KeyJWT()));
                    $arr_response['table'] = substr($name, 0, strlen($name) -1);
                    if (array_key_exists("str_object", $arr_parameter[0])) {
                        $object = New $arr_parameter[0]['str_object']();
                        if ($stId != null) {
                            $name = $stId;
                        }
                        $arr_response['data'] = $db->Select($object->QueryContruct($method, $arr_parameter[0]['str_data_function'], $arr_parameter[0]['str_data_parameters'], $rawData, $name));
                    } else {
                        $arr_response['data'][0] = $arr_parameter[0];
                    }
                    $int_set_header = 200;
                } else {
                    $int_set_header = 404;
                    $arr_response = array('message' => 'Intento de ingreso invalido');
                }
            } else {
                $int_set_header = 401;
                $arr_response = array('message' => $obj_security[1]);
            }
        } else {
            $arr_response = array();
            $int_set_header = 200;
        }
        result(json_encode($arr_response), $int_set_header);
    }

    /**
     * handle_opciones_out_method
     * Meneja las periticiones de post, put, get y delete con parametros no requiere JWT
     * @param method string con el metodo de acceso (POST, GET, OPTION, PUT)
     * @param resource string con el nombre del recurso a utilizar
     * @param name string ruta que determina la accion a realizar
     * @return objeto con respuesta del servicio
     */
    function handle_opciones_out_method($method, $resource, $name) {
        // Declaramos los objetos basico de base de datos ($db), seguridad ($security), token ($jwtsecur)
        $db = New Conn();
        $security = New security();
        $rawData = json_decode(file_get_contents("php://input"),true);

        // Hacemos la busqueda del objeto según la opción pasada
        $arr_parameter = array();
        if ($name == "menu") {
            $arr_response['data'] = $db->Select($security->Object('0', "menu"));
        } else {
            //Cuando rawData es null se considera un enlace externo
            if ($rawData == null) {
                $arr_parameter[0]['str_object'] = 'objects';
                $arr_parameter[0]['str_data_function'] = 'fnc_previews';
                $arr_parameter[0]['str_data_parameters'] = 'link';
            } else {
                $arr_parameter = $db->Select($security->Object('0', $rawData['menu']));
            }
            $object = New $arr_parameter[0]['str_object']();
            $arr_response['data'] = $db->Select($object->QueryContruct($method, $arr_parameter[0]['str_data_function'], $arr_parameter[0]['str_data_parameters'], $rawData, $name));
        }
        $int_set_header = 200;
        result(json_encode($arr_response), $int_set_header);
    }

    /**
     * handle_struct_method
     * Retorna la estructura para el armado de las ABM
     * @param method string con el metodo de acceso (POST, GET, OPTION, PUT)+
     * @param name string ruta que determina la accion a realizar
     * @return objeto con respuesta del servicio
     */
    function handle_struct_method($method, $name) {
        // Declaramos los objetos basico de base de datos ($db), seguridad ($security), token ($jwtsecur)
        $db = New Conn();
        $security = New security();
        $jwtsecur = New jwtoken();
        $rawData = json_decode(file_get_contents("php://input"),true);
        if ($rawData == null) {
            $rawData['menu'] = $name;
        }
        $arr_header = apache_request_headers();
        // Verificamos que exista el header con el token
        if ($arr_header and array_key_exists('jwt', $arr_header)) {
            $obj_security = $jwtsecur->GetJWT($arr_header['jwt'], $db->Select($security->KeyJWT()));
            if (gettype($obj_security) == 'object') {
                $arr_security = array("id" => $obj_security->data->id, "user" => $obj_security->data->user);
                $arr_security = $db->Select($security->Verify($arr_security));
                if ($arr_security[0]['result']=='t') {
                    // Hacemos la busqueda del objeto según la opción pasada
                    $arr_parameter = array();
                    $arr_response['jwt'] = $jwtsecur->SetJWT($obj_security->data->id, $obj_security->data->user, $obj_security->data->profile, $db->Select($security->KeyJWT()));
                    $arr_response['table'] = $name;
                    $arr_response['data'] = $db->Select($security->Table($name));
                    $int_set_header = 200;
                } else {
                    $int_set_header = 404;
                    $arr_response = array('message' => 'Intento de ingreso invalido');
                }
            } else {
                $int_set_header = 401;
                $arr_response = array('message' => $obj_security[1]);
            }
        } else {
            $arr_response = array();
            $int_set_header = 200;
        }
        result(json_encode($arr_response), $int_set_header);
    }

    // Obtencion de la ruta
    /**
     * Estructura principal que maneja la ruta
     */
    xdebug_break();
    $uri = $_SERVER['REQUEST_URI'];
    $method = $_SERVER['REQUEST_METHOD'];
    $paths = explode('/', paths($uri));
    array_shift($paths);
    $resource = array_shift($paths);
    // Según la ruta establece la función receptora
    if (($resource == '') || ($resource == 'users')) {
        $name = array_shift($paths);
        if ($method == 'POST') {
	   	    handle_index($method, $name);
	   	} elseif ($method == 'GET') {
	        handle_index($method);
	    }
    } elseif ($resource == 'struct') {
        $name = array_shift($paths);
        handle_struct_method($method, $name);
    } elseif ($resource == 'functions') {
        $name = array_shift($paths);
        $stId = array_shift($paths);
        handle_opciones_method($method, $name, $stId);
    } elseif ($resource == 'Salir') {
        $name = array_shift($paths);
        if (empty($name)) {
            handle_salir($method);
        }
    } elseif ($resource == "outFunctions") {
        $name = array_shift($paths);
        handle_opciones_out_method($method, $resource, $name);
    } else {
        // We only handle resources under 'clients'
        header('HTTP/1.1 404 Not Found');
    }
?>
