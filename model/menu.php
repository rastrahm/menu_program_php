<?php
class menu {
    /**
     * getMenu
     * Traemos solamente la parte superior del menu correspondiente al profile
     * param (profile) => Identifica al perfil del usuario debuelto en security->Select
     * @param profile string con el id del perfil
     * @return string Query de busqueda del menú
    */
    function getMenu($profile) {
        $query = "SELECT * FROM public.fnc_men(".$profile.")";
        return $query;
    }

    /**
     * getSubmenu
     * Traemos los submenus
     * @param profile String Identifica al perfil del usuario
     * @param menu String nombre del menu a buscar sus submenus
     * @return string Query de busqueda del menú
    **/
    function getSubmenu($profile, $menu) {
        $query = "SELECT * FROM public.fnc_men_sub(".$profile.", ".$menu.")";
        return $query;
    }
}

 ?>
