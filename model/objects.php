<?php
class objects {
    /**
     * QueryContruct
     * Construtor de query, tomar en cuenta como manejar el string con los campos correspondientes
     * @param method string determina el tipo de consulta (GET = Select, POST = Insert, PUT = Update)
     * @param function string con nombre de la funcion a utilizar
     * @param fields array con los campos a utiliza
     * @param paranms array con parametros a utiliza
     * @param name string con nombre de la funcion
     * @return array resultado de la busqueda
    */
    function QueryContruct($method, $function, $fields, $paranms, $name) {
        if (($method == "GET") AND ($name == $paranms['menu'])) {
            $str_function = "SELECT * FROM public.".$function.'_'.$method."_ALL(";
            $name = '';
        } else {
            $str_function = "SELECT * FROM public.".$function.'_'.$method."(";
        }
        $i = 0;
        if (($name) && (($paranms == null) OR (count($paranms) == 1))) {
            if (is_numeric($name)) {
                $str_function .= $name;
                $i = $i + 1;
            } else {
                $str_function .= "'".$name."'";
                $i = $i + 1;
            }
        }
        if ($method == "PUT") {
            foreach($paranms as $key1 => $value1) {
                if ($key1 == 'big_id') {
                    $str_function .= $value1;
                    $i = $i + 1;
                }
            }
        } elseif ($method == "DELETE") {
            $str_function .= "'".$paranms['delete']."'";
        }
        if ($paranms) {
            $arr_paranms = explode(",", str_replace("'", "", str_replace("{", "", str_replace("}", "", $fields))));
            $str_separator = "";
            foreach($arr_paranms as $key0 => $value0) {
                if ($i > 0) {
                    $str_separator = ",";
                }
                foreach($paranms as $key1 => $value1) {
                    if ($value0 == $key1) {
                        $str_function .= $str_separator."'". str_replace('&gt;', '>', str_replace('&lt;', '<', $value1))."'";
                    }
                }
                $i = $i + 1;
            }
        }
        $str_function .= ")";
        return $str_function;
    }
}
?>
